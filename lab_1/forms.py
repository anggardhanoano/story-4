from django import forms
from lab_1.models import CommentModel, ProjectModel
import datetime

class CommentForm(forms.ModelForm):
    # <input id="email" type="email" class="validate">
    email = forms.CharField(label="", widget=forms.TextInput(
        attrs = {
                "id" : "email",
                'class' : 'validate',
                'type' : 'email'
                }
    ))
    # <textarea id="textarea1" class="materialize-textarea"></textarea>
    comment = forms.CharField(label="", widget=forms.Textarea(
    attrs = {
            "id" : "textarea1",
            'class' : 'materialize-textarea',
            }
    ))
    class Meta:
        model = CommentModel
        fields = ("email", "comment")

class ProjectForm(forms.ModelForm):
    title = forms.CharField(label="Project Title", widget=forms.TextInput(
        attrs = {
                "id" : "email",
                }
    ))
    category = forms.CharField(label="Project Type", widget=forms.TextInput(
        attrs = {
                "id" : "email",
                }
    ))
    description = forms.CharField(label="Project Type", widget=forms.TextInput(
        attrs = {
                "id" : "email",
                }
    ))
    date_start =  forms.DateField(input_formats=['%Y-%m-%d'], initial=datetime.date.today(),widget=forms.TextInput(attrs=
                                {
                                    'class':'datepicker'
                                }))

    date_end =  forms.DateField(input_formats=['%Y-%m-%d'], initial=datetime.date.today(),widget=forms.TextInput(attrs=
                                {
                                    'class':'datepicker'
                                }))
    class Meta:
        model = ProjectModel
        fields = ('title','date_start','date_end','place','category','description')