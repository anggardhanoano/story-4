from django.urls import path
from .views import *
#url for app
app_name = "home"

urlpatterns = [
    path('', homepage, name='home'),
    path('about', about, name='about'),
    path('education', education, name='education'),
    path('achievement', achievement, name='achievement'),
    path('project', project, name='project'),
    path('comment', comment, name='comment'),
    path('time', timezone),
    path('project_list', project_list, name='project_list'),
    path('delete/<int:pk>', project_delete, name="delete"),
]
