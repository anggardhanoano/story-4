from django.db import models

# Create your models here.
class CommentModel(models.Model):
    email = models.EmailField(max_length=250)
    comment = models.TextField()

    def __str__(self):
        return f"{self.email} comment"

class ProjectModel(models.Model):
    
    title = models.CharField(max_length=200)
    date_start = models.DateField()
    date_end = models.DateField()
    description = models.CharField(default="",max_length=200)
    place = models.CharField(max_length=200)
    category = models.CharField(max_length=100)

    def __str__(self):
        return f"{self.title}"