from django.shortcuts import render, redirect
from lab_1.forms import CommentForm, ProjectForm
from lab_1.models import CommentModel, ProjectModel
import datetime

def homepage(request):
    now = datetime.datetime.now()
    return render(request, 'homepage.html')
def about(request):
    return render(request, 'about.html')

def education(request):
    return render(request, 'education.html')

def achievement(request):
    return render(request, 'achievment.html')

def project(request):
    return render(request, 'project.html')

def comment(request):
    form = CommentForm(request.POST)
    if request.method == "POST" and form.is_valid():
        instance = form.save()
        return redirect('home:home')
    return render(request, 'comment.html', {
        "form" : CommentForm(),
        "comment" : CommentModel.objects.all(),
    })
def timezone(request):
    now = datetime.datetime.now()
    print(now)
    return render(request, '../templates/base.html', {
        'time':now, "test":"Hello",
    })

def project_list(request):
    form = ProjectForm(request.POST)
    if request.method == 'POST':
        if form.is_valid():
            instance = form.save()
            print(request.POST)
            return redirect('home:project_list')
    return render(request, 'story5.html', {
        'form':ProjectForm(),
        'project': ProjectModel.objects.all(),
    })

def project_delete(request,pk):
    project = ProjectModel.objects.get(pk=pk)
    if request.method == "POST" :
        project.delete()
        return redirect("home:project_list") 
